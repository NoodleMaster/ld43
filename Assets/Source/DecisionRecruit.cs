﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DecisionRecruit : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Text text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.text = "Not letting compassion influence our recruit selection allows us to recruit up to 90% of the civilian population.";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.text = "Decisions:";
    }
}
