﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region : MonoBehaviour
{
    private int id;
    public int GetID() { return id; }

    private bool isOccupied = false;
    public void SetOccupied(bool value)
    {
        isOccupied = value;
        if(isOccupied)
        {
            control = 0f;
            spriteRenderer.sprite = pSprite;
        }
        else
            spriteRenderer.sprite = gSprite;
    }
    public bool GetOccupied()
    {
        return isOccupied;
    }


    private string regionName;
    private int maxFactories;
    private int maxDefense;
    private int recruitable;
    private int civilians;

    private int alienTroops = 0;


    //1 factory produces 10 material
    // and needs 10 humans
    private int defenses = 5;
    private int factories = 1;
    private int upgrades = 2;

    private bool isContested = false;
    public bool wascontested = false;
    public bool GetContested()
    {
        return isContested;
    }
    private float control = 1f;

    private SpriteRenderer spriteRenderer;
    private GameHandler game;
    private Region[] neighbors;


    Sprite rSprite;
    Sprite gSprite;
    Sprite pSprite;

    public void LoadRegion(int id, int humans, int maxFactories, string name)
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        game = FindObjectOfType<GameHandler>();

        this.id = id;
        this.regionName = name;
        this.civilians = humans;
        this.recruitable = civilians / 2;
        this.maxFactories = maxFactories;
        this.maxDefense = civilians / 2;

        string idString = id.ToString("00");
        rSprite = Resources.Load<Sprite>("Graphix/R" + idString);
        gSprite = Resources.Load<Sprite>("Graphix/G" + idString);
        pSprite = Resources.Load<Sprite>("Graphix/P" + idString);

        if(rSprite == null || gSprite == null || pSprite == null)
        {
            Debug.LogError("Could not load all sprites for region: " + idString);
            return;
        }
        spriteRenderer.sprite = gSprite;

        spriteRenderer.color = Color.grey;

        this.gameObject.AddComponent<PolygonCollider2D>();
    }

    public void SetColor(Color color)
    {
        spriteRenderer.color = color;
    }

    private void OnMouseEnter()
    {
        game.SetMouseOverRegion(this);
        switch(game.GetGameMode())
        {
            case GameHandler.GameMode.NORMAL:
                break;

            case GameHandler.GameMode.BUILD:
                SetColor(Color.white);
                break;
        }
    }

    private void OnMouseExit()
    {
        game.SetMouseOverRegion(null);
        switch(game.GetGameMode())
        {
            case GameHandler.GameMode.NORMAL:
                break;

            case GameHandler.GameMode.BUILD:
                SetColor(Color.grey);
                break;
        }
    }

    private void OnMouseUp()
    {
        switch(game.GetGameMode())
        {
            case GameHandler.GameMode.NORMAL:
                game.SetSelectedRegion(this);
                break;

            case GameHandler.GameMode.BUILD:
                if(isOccupied)
                    break;
                switch(game.GetBuildMode())
                {
                    case GameHandler.BuildMode.DEFENSE:
                        BuildDefense();
                        break;

                    case GameHandler.BuildMode.FACTORY:
                        BuildFactory();
                        break;

                    case GameHandler.BuildMode.UPGRADE:
                        BuildUpgrade();
                        break;
                }
                break;
        }
    }


    private bool BuildDefense()
    {
        if(GetRegionRecruiteable() <= 0)
            return false;

        if(game.SubtractMaterials(GetDefensePrice()))
        {
            civilians--;
            defenses++;
            return true;
        }
        return false;
    }
    private bool BuildFactory()
    {
        if(factories >= maxFactories)
            return false;

        if(game.SubtractMaterials(GetFactoryPrice()))
        {
            factories++;
            return true;
        }
        return false;
    }
    private bool BuildUpgrade()
    {
        if(game.SubtractMaterials(GetUpgradePrice()))
        {
            upgrades++;
            return true;
        }
        return false;
    }
    public int GetDefensePrice()
    {//Balance this
        int price = 10;
        return price;
    }
    public int GetFactoryPrice()
    {//Balance this
        int price = (int)(1 * Mathf.Pow(1.7f, upgrades-1) * (factories+1 / 2));
        return price;
    }
    public int GetUpgradePrice()
    {//Balance this
        int price = (int)(factories * Mathf.Pow(1.7f, upgrades));
        return price;
    }

    public void SetNeighbors(Region[] neighbors)
    {
        this.neighbors = neighbors;
    }
    
    public void SetRecruitDec()
    {
        recruitable += (int)(civilians * 0.4f);
    }

    public string GetRegionName()
    {
        return regionName;
    }
    public int GetCivilianPopulation()
    {
        return civilians;
    }
    public void SetCivilianPopulation(int value)
    {
        civilians = value;
    }
    public int GetRegionMaxFactories()
    {
        return maxFactories;
    }
    public int GetRegionProd()
    {
        int producedMaterials = Mathf.FloorToInt((float)(factories * upgrades * 20) * control);
        producedMaterials *= game.workDecision ? 2 : 1;

        return producedMaterials - (int)(5 + Mathf.Pow(1.17f, defenses));
    }
    public int GetRegionOccupiedProd()
    {
        return factories * upgrades * 10;
    }
    public int GetRegionRecruiteable()
    {
        return recruitable - defenses;
    }

    public int GetRegionFactories()
    {
        return factories;
    }
    public int GetRegionUpgrades()
    {
        return upgrades;
    }
    public float GetRegionControl()
    {
        return control;
    }
    public int GetRegionDefense()
    {
        return defenses;
    }
    public void SetRegionDefenses(int def)
    {
        defenses = def;
    }
    public void SetAlienTroops(int value)
    {
        alienTroops = value;
    }


    public void Step()
    {
        recruitable = Mathf.Max(Mathf.Min(recruitable, (int)(civilians * (game.recrDecision ? 0.9f : 0.5f))), 0);

        if(wascontested)
        {
            wascontested = false;
        }

        

        if(GetOccupied())
        {
            game.humansKilled += Mathf.Max(1, GetCivilianPopulation() / 20);
            alienTroops += Mathf.Max(1, GetCivilianPopulation() / 20);
            civilians -= GetCivilianPopulation() / 20;

            //Debug.Log(regionName + " has " + alienTroops.ToString() + " zombies");
        }
        else
        {
            if(game.prioDecision)
            {
                game.humanKilledByDec += Mathf.Max(1, civilians / 100);
                civilians -= Mathf.Max(1, civilians / 100);
            }
            if(game.workDecision)
            {
                game.humanKilledByDec += Mathf.Max(1, civilians / 100);
                civilians -= Mathf.Max(1, civilians / 100);
            }
        }




        if(!isContested)
            return;

        control = Mathf.Min((defenses / (float)alienTroops) / 2f, 0.95f);
        int damageHumans = Mathf.Max((int)(defenses * game.GetDamageModifier() / 10), 1);
        damageHumans *= game.prioDecision ? 2 : 1;

        int damageAliens = Mathf.Max((int)(alienTroops * Alien.damageModifier / 10), 1);
        
        defenses -= damageAliens;
        alienTroops -= damageHumans;

        if(defenses <= 0)
        {
            defenses = 0;
            control = 0f;
            SetOccupied(true);
            StopContesting();
        }
        else if(alienTroops <= 0)
        {
            alienTroops = 0;
            control = 1f;
            SetOccupied(false);
            StopContesting();
        }
        game.zombiesKilled += damageHumans;
        game.humansKilled += damageAliens;
    }

    public void StartContesting(int amountTroops)
    {
        isContested = true;
        alienTroops = amountTroops;
        spriteRenderer.sprite = rSprite;
        control = Mathf.Min((defenses / (float)alienTroops) / 2f, 0.95f);
    }


    public void StopContesting()
    {
        if(isOccupied)
        {
            game.AddRegionToAlien(this);
            if(game.leftDecision)
            {
                Debug.LogWarning("Sacrificed " + civilians + " humans in " + regionName);
                game.humanKilledByDec += civilians;
                civilians = 0;
            }
        }
        isContested = false;
        wascontested = true;
    }


    public bool CanBuildFactory()
    {
        return factories < maxFactories;
    }


    public void AddFactory()
    {
        factories++;
    }
    public void AddUpgrade()
    {
        upgrades++;
    }


    public int GetAlienTroops()
    {
        return alienTroops;
    }
}
