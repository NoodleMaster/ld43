﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DecisionWork : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Text text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.text = "Overworking the civilian population yields higher productivity, but any workplace safety is nonexistant.";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.text = "Decisions:";
    }
}
