﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DecisionPriority : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Text text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.text = "Prioritizing resources in favor of the military means starving civilians, but more damage against zombies.";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.text = "Decisions:";
    }
}
