﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : MonoBehaviour
{
    public Text thankText;

    bool quit = false;
    float counter = 0;

    public void Play()
    {
        SceneManager.LoadScene("MainScene");
    }


    public void Quit()
    {
        thankText.gameObject.SetActive(true);

        quit = true;
    }


    private void Update()
    {
        if(quit)
        {
            counter += Time.deltaTime;

            if(counter >= 1.5f)
            {
                Application.Quit();
            }
        }
    }
}
