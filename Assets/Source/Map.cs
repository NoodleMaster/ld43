﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    private readonly string[] regionNames = { "Ugror", "Kostaosal", "Ugrurg", "SRA-2", "SRA-1", "Extrus", "Zocrium", "Swuyrhiel", "Creussau", "STA-1", "Oplad", "Oplium", "Eprait", "Astrua", "Josmuisia", "Traoton", "Clule", "Triadan", "Nsadrea" };

    private Region[] allRegions = new Region[19];

    public Region regionPrefab;

    public int totalHumans = 0;

    void Start ()
    {
        //one human unit = 1000000 humans
        
        for(int i = 0; i < allRegions.Length; i++)
        {
            Region r = Instantiate(regionPrefab, this.transform);
            allRegions[i] = r;

            //Generate the potentially extracteable materials
            // and amount of humans living here
            int humanshere = Random.Range(10, 501);
            totalHumans += humanshere;
            int maxFactories = Random.Range(5, 31);

            r.LoadRegion(i, humanshere, maxFactories, regionNames[i]);
            r.SetNeighbors(GetNeighbors(i));
        }

        Debug.Log(totalHumans);

        for(int i = 0; i < allRegions.Length; i++)
        {
            allRegions[i].SetNeighbors(GetNeighbors(i));
        }
    }


    public Region[] GetAllRegions()
    {
        return allRegions;
    }


    public Region[] GetNeighbors(int id)
    {
        switch(id)
        {
            case 0: return new Region[] { allRegions[1], allRegions[2] };
            case 1: return new Region[] { allRegions[0], allRegions[3] };
            case 2: return new Region[] { allRegions[0], allRegions[3], allRegions[6] };
            case 3: return new Region[] { allRegions[1], allRegions[2], allRegions[4], allRegions[6], allRegions[7] };
            case 4: return new Region[] { allRegions[3], allRegions[7], allRegions[12] };
            case 5: return new Region[] { allRegions[6] };
            case 6: return new Region[] { allRegions[5], allRegions[7], allRegions[8] };
            case 7: return new Region[] { allRegions[6], allRegions[3], allRegions[4], allRegions[8], allRegions[9] };
            case 8: return new Region[] { allRegions[6], allRegions[7], allRegions[10] };
            case 9: return new Region[] { allRegions[7], allRegions[10], allRegions[12], allRegions[13], allRegions[11] };
            case 10: return new Region[] { allRegions[8], allRegions[9], allRegions[11] };
            case 11: return new Region[] { allRegions[10], allRegions[9], allRegions[13] };
            case 12: return new Region[] { allRegions[4], allRegions[9], allRegions[13], allRegions[14], allRegions[15] };
            case 13: return new Region[] { allRegions[11], allRegions[9], allRegions[12], allRegions[14] };
            case 14: return new Region[] { allRegions[13], allRegions[12], allRegions[15], allRegions[16] };
            case 15: return new Region[] { allRegions[12], allRegions[14], allRegions[16] };
            case 16: return new Region[] { allRegions[15], allRegions[14], allRegions[17], allRegions[18] };
            case 17: return new Region[] { allRegions[16], allRegions[18] };
            case 18: return new Region[] { allRegions[16], allRegions[17] };

            default: Debug.LogError("this id is unknown: " + id); return null;
        }
    }
}
