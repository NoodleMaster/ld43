﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class DecisionLeftover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Text text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.text = "Burning every civilan in a region that is about to be lost ensures that there are fewer zombies we have to fight.";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.text = "Decisions:";
    }
}
