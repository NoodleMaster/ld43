﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text time;
    public Text zombie;
    public Text human;
    public Text dec;
    public Text score;


    void Start ()
    {
        GameHandler handler = FindObjectOfType<GameHandler>();

        time.text = "You survived for " + handler.stepCounter.ToString() + " days.";
        zombie.text = "Zombies killed: " + handler.zombiesKilled.ToString() + " million";
        human.text = "Humans killed by zombies:\n" + handler.humansKilled.ToString() + " million";
        dec.text = "Humans killed by your decisions:\n" + handler.humanKilledByDec.ToString() + " million";
        score.text = "Final Score: " + (handler.stepCounter * 1000 + handler.zombiesKilled * 10 - handler.humanKilledByDec * 10).ToString();
    }


    public void ToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
    public void ToMain()
    {
        SceneManager.LoadScene("MainScene");
    }
}
