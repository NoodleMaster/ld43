﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien
{
    public static float damageModifier = 1;

    List<Region> occupiedRegions = new List<Region>();

	public Alien(Map map)
    {
        //pick a SRA-1, SRA-2, or STA-1 to start

        switch(Random.Range(0, 3))
        {
            case 0: occupiedRegions.Add(map.GetAllRegions()[3]); break;
            case 1: occupiedRegions.Add(map.GetAllRegions()[4]); break;
            case 2: occupiedRegions.Add(map.GetAllRegions()[9]); break;
        }

        occupiedRegions[0].SetOccupied(true);
        occupiedRegions[0].SetAlienTroops(occupiedRegions[0].GetCivilianPopulation() / 3);
        occupiedRegions[0].SetCivilianPopulation(occupiedRegions[0].GetCivilianPopulation() - occupiedRegions[0].GetCivilianPopulation() / 3);
    }

    public void AddRegion(Region region)
    {
        occupiedRegions.Add(region);
    }



    public void Step(Map map)
    {
        damageModifier *= 1.05f;
        List<Region> potentialExpandRegions = new List<Region>();

        foreach(Region region in occupiedRegions)
        {
            Region[] neighbors = map.GetNeighbors(region.GetID());

            foreach(Region neighbor in neighbors)
            {
                if(neighbor.wascontested == false && neighbor.GetContested() == false && neighbor.GetOccupied() == false && potentialExpandRegions.Contains(neighbor) == false)
                {
                    potentialExpandRegions.Add(neighbor);
                }
            }
        }



        foreach(Region region in potentialExpandRegions)
        {
            int availableTroops = 0;
            foreach(Region neighbor in map.GetNeighbors(region.GetID()))
            {
                if(neighbor.GetOccupied())
                {
                    foreach(Region neighborneighbor in map.GetNeighbors(neighbor.GetID()))
                    {
                        if(neighborneighbor.GetOccupied())
                        {
                            int neighbortroops = neighborneighbor.GetAlienTroops() / 4;
                            if(neighbortroops <= 0)
                                continue;

                            availableTroops += neighbortroops;
                            neighborneighbor.SetAlienTroops(neighborneighbor.GetAlienTroops() / 4);
                        }
                    }
                    int troops = neighbor.GetAlienTroops() / 2;
                    if(troops <= 0)
                        continue;

                    availableTroops += troops;
                    neighbor.SetAlienTroops(neighbor.GetAlienTroops() / 2);
                }
            }
            if(availableTroops <= 0)
                continue;

            region.StartContesting(availableTroops);
        }
    }
}
