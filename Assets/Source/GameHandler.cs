﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public enum GameMode { NORMAL, BUILD }
    private GameMode gameMode;

    public enum BuildMode { DEFENSE, FACTORY, UPGRADE }
    private BuildMode buildMode;


    public int humanKilledByDec = 0;
    public bool workDecision = false;
    public bool prioDecision = false;
    public bool leftDecision = false;
    public bool recrDecision = false;
    public void EnableWorkDecision()
    {
        workDecision = true;
        buttonWorkj.gameObject.GetComponent<Image>().color = Color.green;
        buttonWorkj.interactable = false;
    }
    public void EnablePrioDecision()
    {
        prioDecision = true;
        buttonPrio.gameObject.GetComponent<Image>().color = Color.green;
        buttonPrio.interactable = false;
    }
    public void EnableLeftDecision()
    {
        leftDecision = true;
        buttonLeft.gameObject.GetComponent<Image>().color = Color.green;
        buttonLeft.interactable = false;
    }
    public void EnableRecrDecision()
    {
        recrDecision = true;
        buttonRecr.gameObject.GetComponent<Image>().color = Color.green;
        buttonRecr.interactable = false;

        foreach(Region region in map.GetAllRegions())
        {
            region.SetRecruitDec();
        }
    }


    public int zombiesKilled = 0;
    public int humansKilled = 0;
    bool gameOver = false;

    private bool paused = true;
    private float timer = 0f;
    private float timeStep = 1f;
    public int stepCounter = 0;

    private int availableMaterials;
    private float damageModifier = 1f;
    public float GetDamageModifier() { return damageModifier; }

    private Alien alien;


    public Button buttonLeft;
    public Button buttonPrio;
    public Button buttonWorkj;
    public Button buttonRecr;

    private Region selectedRegion; 
    private Region mouseOverRegion;
    private Map map;

    private GameObject pausedText;
    private Text daysText;
    private Text matText;
    private Text IncomeText;

    private GameObject rInfoUI;
    private Text rName;
    private Text rPop;
    private Text rProd;
    private Text rFac;
    private Text rCont;
    private Text rDef;
    private Text rMil;

    private GameObject mouseOverUI;
    private Text[] mouseOverTexts;

    private GameObject buildSelect;
    private Image defBuildToggle;
    private Image facBuildToggle;
    private Image upgBuildToggle;

    private GameObject contestedInfoUI;
    private Slider controlBar;
    private Text alienTroopsText;
    private Text defensesText;



    void Start ()
    {
        GameObject.DontDestroyOnLoad(this.gameObject);

        map = FindObjectOfType<Map>();

        pausedText = GameObject.Find("Text_Paused");
        daysText = GameObject.Find("Text_Time").GetComponent<Text>();
        matText = GameObject.Find("Text_TotalMaterials").GetComponent<Text>();
        IncomeText = GameObject.Find("Text_TotalMaterialIncome").GetComponent<Text>();
        
        rInfoUI = GameObject.Find("Region_Info");
        rName = GameObject.Find("Text_Name").GetComponent<Text>();
        rPop = GameObject.Find("Text_Pop").GetComponent<Text>();
        rProd = GameObject.Find("Text_Prod").GetComponent<Text>();
        rFac = GameObject.Find("Text_Fac").GetComponent<Text>();
        rCont = GameObject.Find("Text_Cont").GetComponent<Text>();
        rDef = GameObject.Find("Text_Def").GetComponent<Text>();
        rMil = GameObject.Find("Text_Mil").GetComponent<Text>();

        mouseOverUI = GameObject.Find("MouseOverUI");
        mouseOverTexts = GameObject.Find("MouseOverUI").GetComponentsInChildren<Text>();

        buildSelect = GameObject.Find("BuildSelect");
        defBuildToggle = GameObject.Find("Select_Def").GetComponent<Image>();
        facBuildToggle = GameObject.Find("Select_Fac").GetComponent<Image>();
        upgBuildToggle = GameObject.Find("Select_Fac_Up").GetComponent<Image>();

        contestedInfoUI = GameObject.Find("Contested_InfoUI");
        controlBar = GameObject.Find("Slider").GetComponent<Slider>();
        alienTroopsText = GameObject.Find("Text_AlienTroops").GetComponent<Text>();
        defensesText = GameObject.Find("Text_HumanTroops").GetComponent<Text>();


        //--init stuff--
        SetBuildDefense();
        SetGameMode(GameMode.NORMAL);

        SetMouseOverRegion(null);
        SetSelectedRegion(null);
    }


    public bool SubtractMaterials(int amount)
    {
        if(availableMaterials >= amount)
        {
            availableMaterials -= amount;
            return true;
        }
        else
        {
            //play error sound
            return false;
        }
    }


    private void TogglePaused()
    {
        paused = !paused;
        pausedText.SetActive(paused);
    }


    void Update ()
    {
        if(gameOver)
        {
            Destroy(this.gameObject);
            return;
        }

        gameOver = true;
        foreach(Region region in map.GetAllRegions())
        {
            if(region.GetOccupied() == false)
                gameOver = false;
        }

        if(gameOver)
        {
            foreach(Region region in map.GetAllRegions())
            {
                humansKilled += region.GetCivilianPopulation();
            }
            SceneManager.LoadScene("GameOverScene");
        }


        UpdateGeneralInfoUI();
        UpdateMouseOverRegionInfoUI();
        UpdateRegionInfoUI();
        UpdateContestedInfoUI();

        if(Input.GetKeyUp(KeyCode.Space))
            TogglePaused();

        if(paused)
            return;

        timer += Time.deltaTime;
        if(timer >= timeStep)
        {
            timer = 0;
            GameStep();
        }
	}


    private void GameStep()
    {
        Debug.Log("dec: " + humanKilledByDec);
        Debug.Log("zom: " + humansKilled);
        foreach(Region region in map.GetAllRegions())
        {
            availableMaterials += region.GetRegionProd();
            region.Step();
        }


        if(stepCounter == 0)
            alien = new Alien(map);
        else
            alien.Step(map);
        
        stepCounter++;
    }
    
    public void AddRegionToAlien(Region region)
    {
        alien.AddRegion(region);
    }


    public void SetMouseOverRegion(Region region)
    {
        mouseOverRegion = region;
        UpdateMouseOverRegionInfoUI();
        if(gameMode == GameMode.BUILD)
        {
            SetSelectedRegion(mouseOverRegion);
        }
    }
    public Region GetMouseOverRegion()
    {
        return mouseOverRegion;
    }
    
    public void SetSelectedRegion(Region region)
    {
        if(selectedRegion != null)
            selectedRegion.SetColor(Color.grey);

        selectedRegion = region;
        if(selectedRegion != null)
            selectedRegion.SetColor(Color.white);

        UpdateRegionInfoUI();
    }
    public Region GetSelectedRegion()
    {
        return selectedRegion;
    }


    public void ToggleGameModeBuild()
    {
        if(GetGameMode() == GameMode.BUILD)
            SetGameMode(GameMode.NORMAL);
        else
            SetGameMode(GameMode.BUILD);
    }

    public void SetGameMode(GameMode mode)
    {
        this.gameMode = mode;
        if(selectedRegion != null)
        {
            selectedRegion.SetColor(Color.grey);
            selectedRegion = null;
        }
        if(gameMode != GameMode.BUILD)
            buildSelect.SetActive(false);
        else
            buildSelect.SetActive(true);
    }
    public GameMode GetGameMode()
    {
        return gameMode;
    }


    public void SetBuildDefense()
    {
        SetBuildMode(BuildMode.DEFENSE);
        defBuildToggle.color = Color.white;
        facBuildToggle.color = Color.grey;
        upgBuildToggle.color = Color.grey;
    }
    public void SetBuildFactory()
    {
        SetBuildMode(BuildMode.FACTORY);
        defBuildToggle.color = Color.grey;
        facBuildToggle.color = Color.white;
        upgBuildToggle.color = Color.grey;
    }
    public void SetBuildUpgrade()
    {
        SetBuildMode(BuildMode.UPGRADE);
        defBuildToggle.color = Color.grey;
        facBuildToggle.color = Color.grey;
        upgBuildToggle.color = Color.white;
    }
    public void SetBuildMode(BuildMode mode)
    {
        buildMode = mode;
    }
    public BuildMode GetBuildMode()
    {
        return buildMode;
    }


    private void UpdateGeneralInfoUI()
    {
        daysText.text = "Day " + stepCounter.ToString();
        matText.text = "$" + availableMaterials.ToString();
        int income = 0;
        foreach(Region region in map.GetAllRegions())
            income += region.GetRegionProd();

        IncomeText.text = "Prod.: " + income.ToString();
    }
    
    private void UpdateRegionInfoUI()
    {
        if(selectedRegion == null)
        {
            rInfoUI.SetActive(false);
            return;
        }
        rInfoUI.SetActive(true);
        if(selectedRegion.GetOccupied())
        {
            rName.text = selectedRegion.GetRegionName();
            rPop.text = "Civilians: ???";
            rProd.text = "Production: $0";
            rFac.text = "Factories: " + selectedRegion.GetRegionFactories().ToString() + " / " + selectedRegion.GetRegionMaxFactories().ToString();
            rCont.text = "Control: " + (selectedRegion.GetRegionControl() * 100).ToString("##0") + "%";
            rDef.text = "Defenses: 0";
            rMil.text = "Recruiteable: 0";
        }
        else
        {
            rName.text = selectedRegion.GetRegionName();
            rPop.text = "Civilians: " + selectedRegion.GetCivilianPopulation().ToString() + " million";
            rProd.text = "Production: " + "$" + selectedRegion.GetRegionProd().ToString();
            rFac.text = "Factories: " + selectedRegion.GetRegionFactories().ToString() + " / " + selectedRegion.GetRegionMaxFactories().ToString();
            rCont.text = "Control: " + (selectedRegion.GetRegionControl() * 100).ToString("##0") + "%";
            rDef.text = "Defenses: " + selectedRegion.GetRegionDefense() + " million";
            rMil.text = "Recruiteable: " + Mathf.Max(0, selectedRegion.GetRegionRecruiteable()).ToString() + " million";
        }
    }

    private void UpdateMouseOverRegionInfoUI()
    {
        if(mouseOverRegion == null)
        {
            mouseOverUI.SetActive(false);
            return;
        }
        mouseOverUI.SetActive(true);

        string displaytext1 = "";
        string displaytext2 = "";
        switch(GetGameMode())
        {
            case GameHandler.GameMode.NORMAL:
                displaytext1 = mouseOverRegion.GetRegionName();
                break;

            case GameHandler.GameMode.BUILD:
                if(mouseOverRegion.GetOccupied())
                {
                    displaytext1 = "Occupied by zombies.";
                    break;
                }
                switch(buildMode)
                {
                    case BuildMode.DEFENSE:
                        displaytext1 = "Defense: " + mouseOverRegion.GetRegionDefense();
                        displaytext2 = "$" + mouseOverRegion.GetDefensePrice().ToString();
                        break;
                    case BuildMode.FACTORY:
                        displaytext1 = "Factories: " + mouseOverRegion.GetRegionFactories() + " / " + mouseOverRegion.GetRegionMaxFactories();
                        displaytext2 = "$" + mouseOverRegion.GetFactoryPrice().ToString();
                        break;
                    case BuildMode.UPGRADE:
                        displaytext1 = "Upgrades: " + mouseOverRegion.GetRegionUpgrades();
                        displaytext2 = "$" + mouseOverRegion.GetUpgradePrice().ToString();
                        break;
                }
                break;
        }
        mouseOverTexts[0].text = displaytext1;
        mouseOverTexts[1].text = displaytext2;
    }


    private void UpdateContestedInfoUI()
    {
        if(selectedRegion == null || selectedRegion.GetContested() == false)
        {
            contestedInfoUI.SetActive(false);
            return;
        }
        contestedInfoUI.SetActive(true);

        controlBar.value = 1 - selectedRegion.GetRegionControl();
        alienTroopsText.text = "Zombies: " + selectedRegion.GetAlienTroops() + " million";
        defensesText.text = "Defenses: " + selectedRegion.GetRegionDefense() + " million";

    }


    public void ReinforceSelectedRegion()
    {
        if(selectedRegion == null || selectedRegion.GetOccupied() || selectedRegion.GetContested() == false)
            return;

        foreach(Region region in map.GetNeighbors(selectedRegion.GetID()))
        {
            if(region.GetOccupied())
                continue;

            int reinforcements = region.GetRegionDefense() / 5;

            region.SetRegionDefenses(region.GetRegionDefense() - reinforcements);

            selectedRegion.SetRegionDefenses(selectedRegion.GetRegionDefense() + reinforcements);
        }
    }
}
